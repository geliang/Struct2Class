#ifndef __MATCH_VS_STRUCT_H__
#define __MATCH_VS_STRUCT_H__


#include <MsString.h>

#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__))
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __attribute__((visibility("default")))
#endif

/** @brief 最大拓展字段个数 */
#define		MS_EXTEND_MAX_SIZE			32

/** @brief 最大用户个数 */
#define		MS_USER_MAX_SIZE				128

/** @brief MatchVS外部事件最小值（有效值） */
#define		MS_EVENT_ID_MIN					2000
/** @brief MatchVS外部事件最大值（有效值） */
#define		MS_EVENT_ID_MAX					3000


typedef int Int32;


namespace matchvs
{
	/* @brief 加入房间flag枚举 */
	typedef enum MatchVSJoinRoomFlags
	{
		/** @brief 随机加入 */
		MATCHVS_ROOM_RANDOM = 0x01,
		/** @brief 房间不存在创建 */
		MATCHVS_ROOM_NOT_EXISTS_THEN_CREATE = 0x02,
		/** @brief 附带房间名字 */
		MATCHVS_ROOM_WITH_NAME = 0x04,
		/** @brief 附带负载 */
		MATCHVS_ROOM_WITH_PAYLOAD = 0x08
	}MatchVSJoinRoomFlags;

	/* @brief 创建房间flag枚举 */
	typedef enum MatchVSCreateRoomFlags
	{
		/** @brief 可以随机加入 */
		MATCHVS_CREATE_ROOM_RANDOM = 0x01,
		/** @brief  指定房间名字 */
		MATCHVS_CREATE_ROOM_WITH_NAME = 0x04,
		/** @brief  附带负载 */
		MATCHVS_CREATE_ROOM_WITH_PAYLOAD = 0x08
	}MatchVSCreateRoomFlags;
    /** @brief 用户动作状态 */
    typedef enum MsActionStatus
    {
        /** @brief 不在房间 */
        STATUS_NOT_IN = -1,
        /** @brief 在房间 */
        STATUS_IN = 0,
        /** @brief 准备已就绪 */
        STATUS_READY = 1,
        /** @brief 游戏已开始 */
        STATUS_START = 2
    }MsActionStatus;
    typedef enum GameAction
    {
        MS_GAME_ACTION_READY = 1,//2
        MS_GAME_ACTION_START = 3,//4,
        MS_GAME_ACTION_CANCEL_READY = 5,//6,
        MS_GAME_ACTION_OVER = 7//8
    }GameAction;

	/**
	* @brief  用户信息类
	*/
	typedef struct MsUserInfo
	{
		/** @brief 用户id */
		int									userID;
		/** @brief 用户的验证授权 */
		MsString							token;
		/** @brief 用户昵称 */
		MsString							nickname;
		/** @brief 用户头像地址 */
		MsString							headerUri;
		
		MsUserInfo():userID(0){}
	}MsUserInfo;

	typedef struct MsUserInfoRsp
	{
		/** @brief 返回状态 */
		int									status;
		/** 请求类型 */
		int									type;
		/** @brief 用户信息 */
		MsUserInfo						userInfo;

		MsUserInfoRsp() :status(0) {}
	}MsUserInfoRsp;

	typedef struct MsRegist {
		/** @brief 服务器mac地址 */
		MsString							MAC;
		/** @brief 服务器设备id */
		MsString							deviceID;
		MsString							channel;
		MsString							pid;
	}MsRegist;

	typedef struct MsRegistRsp {
		/** @brief 返回的数据的状态 */
		int							status;
		/** @brief 用户id */
		int							userID;
		/** @brief 返回给用户的验证授权 */
		MsString					token;

		MsRegistRsp() : status(0), userID(0) {}
	}MsRegistRsp;

	/** @brief 服务器信息 */
	typedef struct MsServInfo
	{
		/** @brief 服务器ip */
		MsString						ip;
		/** @brief 服务器端口 */
		int								port;
		/** @brief 服务器id */
		int								serverID;
		/** @brief 用户数 */
		int								userCount;

		MsServInfo() : port(0), serverID(0), userCount(0) {}

	}MsServInfo;

	/** @brief 大厅服务器信息获取 */
	typedef struct MsLobbyServInfo
	{
		/** @brief 游戏id */
		int								gameID;
		/** @brief 场id */
		int								fieldID;
		/** @brief sdk版本号 */
		int								version;

		MsLobbyServInfo() : gameID(0), fieldID(0), version(0) {}
	}MsLobbyServInfo;

	/** @brief 大厅服务器信息回复 */
	typedef struct MsLobbyServInfoRsp
	{
		/** @brief 服务器数 */
		int								serverCount;	
		/** @brief 服务器信息 */
		MsServInfo					serverInfoList[2];
	
		MsLobbyServInfoRsp() : serverCount(0) {}
	}MsLobbyServInfoRsp;

	/** @brief 登陆请求 */
	typedef struct MsLogin
	{
		/** @brief 用户id */
		int								userID;
		/** @brief 游戏id */
		int								gameID;
		/** @brief 游戏version */
		int								gameVersion;
		/** @brief 游戏app_key */
		MsString						app_key;
		/** @brief 服务器id */
		int								serverID;
		/** @brief 用户秘钥 */
		MsString						userToken;

		MsLogin() : userID(0), gameID(0), serverID(0) {}
	}MsLogin;

	/** @brief 登陆回复 */
	typedef struct MsLoginRsp
	{
		/** @brief 登陆状态 */
		int								status;
		/** @brief 用户ID */
		int								userID;
		/** @brief 用户名 */
		MsString						name;
		/** @brief 头像地址 */
		MsString						headerUri;
		
		MsLoginRsp() : status(0),userID(0) {}
	}MsLoginRsp;

	/** @brief 退出回复 */
	typedef struct MsLogoutRsp
	{
		/** @brief 退出状态 */
		int								status;

		MsLogoutRsp() : status(0) {}
	}MsLogoutRsp;

	/** @brief 创建房间请求 */
	typedef struct MsRoomCreate
	{
		/** @brief 最大用户数 */
		int								maxUsers;
		/** @brief 标签 */
		int								flags;
		/** @brief 房间名 */
		MsString						roomName;
		/** @brief 负载数据 */
		MsString						payload;

		MsRoomCreate() : maxUsers(0), flags(0) {}
	}MsRoomCreate;

	/** @brief 创建房间回复 */
	typedef struct MsRoomCreateRsp
	{
		/** @brief 状态 */
		int								status;
		/** @brief 房间id */
		int								roomID;
		/** @brief 消息 */
		MsString						msg;

		MsRoomCreateRsp() : status(0), roomID(0) {}
	}MsRoomCreateRsp;

	/** @brief 进入房间 */
	typedef struct MsRoomJoin
	{
		/** @brief 场id */
		int								fieldID;
		/** @brief 标签 */
		int								flags;
		/** @brief 房间名 */
		MsString						roomName;
		/** @brief 负载数据 */
		MsString						payload;

		MsRoomJoin() : fieldID(0), flags(0) {}
	}MsRoomJoin;

	/** @brief 进入房间回复 */
	typedef struct MsRoomJoinRsp
	{
		/** @brief 进房状态 */
		int								status;
		/** @brief 房间id */
		int								roomID;
		/** @brief 消息 */
		MsString						msg;

		MsRoomJoinRsp() : status(0), roomID(0) {}
	}MsRoomJoinRsp;

	/** @brief 退出房间 */
	typedef struct MsRoomLeave
	{
		/** @brief 负载数据 */
		MsString						payload;
	}MsRoomLeave;

	/** @brief 退出房间回复 */
	typedef struct MsRoomLeaveRsp
	{
		/** @brief 退出状态 */
		int								status;
		/** @brief 房间id */
		int								roomID;
		/** @brief 消息 */
		MsString						msg;
	
		MsRoomLeaveRsp() : status(0), roomID(0) {}
	}MsRoomLeaveRsp;
	
	/** @brief 进房通知 */
	typedef struct MsRoomJoinNotice
	{
		/** @brief 用户id */
		int								userID;
		/** @brief 房间id */
		int								roomID;
		/** @brief 用户名 */
		MsString						userName;
		/** @brief 头像地址 */
		MsString						headerUri;

		MsRoomJoinNotice() : userID(0), roomID(0) {}
	}MsRoomJoinNotice;

	/** @brief 退出房间通知 */
	typedef struct MsRoomLeaveNotice
	{
		/** @brief 用户id */
		int								userID;
		/** @brief 房间id */
		int								roomID;

		MsRoomLeaveNotice() :	userID(0), roomID(0) {}
	}MsRoomLeaveNotice;

	/** @brief 同步用户状态 */
	typedef struct MsUserSyncStatus
	{

		/** @brief 房间ID */
		int							roomId;
		/** @brief 用户ID */
		int							userId;
		/** @brief 房主ID */
		int							ownerId;
		/** @brief 是否为机器人 */
		bool						isRobot;
		/** @brief 用户状态 */
		int							status;
		/** @brief 机器人等级 */
		int							robotGrade;

		MsUserSyncStatus() : roomId(0),userId(0),ownerId(0),isRobot(false),status(0),robotGrade(0) {}
	}MsUserSyncStatus;

	/** @brief 游戏分数 */
	typedef struct MsGameScore
	{
		/** @brief 分数A */
		int 						scoreA;
		/** @brief 分数B */
		int 						scoreB;
		/** @brief 分数C */
		int 						scoreC;
		/** @brief 拓展分数个数 */
		int						extend_num;
		/** @brief 拓展分数列表 */
		int						extend[MS_EXTEND_MAX_SIZE];

		MsGameScore() : scoreA(0), scoreB(0), scoreC(0), extend_num(0)
		{
			memset(extend, 0, sizeof(extend));
		}
	} MsGameScore;

	/** @brief 业务消息 */
	typedef struct MsBusiMsg
	{
		/** @brief 目标用户数 */
		char						targetUserNum;
		/** @brief 标签 */
		char						flags;
		/** @brief 事件ID */
		int							eventID;
		/** @brief 目标用户 */
		int							targetUsers[MS_USER_MAX_SIZE];
		/** @brief 消息大小 */
		int							msgSize;
		/** @brief 消息内容 */
		MsString					msg;
		/** @brief 是否有分数 */
		bool							hasScore;
		/** @brief 分数(由hasScore决定) */
		MsGameScore			score;

		MsBusiMsg() :	targetUserNum(0),flags(0),eventID(0),msgSize(0), hasScore(false)
		{
			memset(targetUsers, 0, sizeof(targetUsers));
		}
	}MsBusiMsg;

	/** @brief 游戏结果下发消息 */
	typedef struct MsGameScoreNotice
	{
		/** @brief 房间ID */
		int							roomID;
		/** @brief 游戏局ID */
		int							roundID;
		/** @brief 用户ID */
		int							userID;
		/** @brief 是否有负载消息 */
		bool							hasPayload;
		/** @brief 负载消息内容 */
		MsString					payload;
		/** @brief 分数列表 */
		MsGameScore			score;

		MsGameScoreNotice() : roomID(0), roundID(0), userID(0), hasPayload(false) {}
	} MsGameScoreNotice;

	/** @brief 业务返回消息 */
	typedef struct MsBusiMsgRsp
	{
		/** @brief 用户id */
		int							userId;
		/** @brief 标签 */
		char							flags;
		/** @brief 事件ID */
		int							eventID;
		/** @brief 消息大小 */
		int							payloadSize;
		/** @brief 是否有负载消息 */
		bool							hasPayload;
		/** @brief 消息内容 */
		MsString					payload;
		/** @brief 是否有分数 */
		bool							hasScore;
		/** @brief 分数 */
		MsGameScoreNotice	score;

		MsBusiMsgRsp() : userId(0), flags(0), eventID(0), payloadSize(0), hasPayload(false), hasScore(false) {}
	}MsBusiMsgRsp;

	/** @brief 其他用户进入房间消息 */
	typedef struct MsGamePeerJoinNotice
	{
		/** @brief 房间ID */
		int							roomID;
		/** @brief 用户ID */
		int							userID;
		/** @brief 用户名字 */
		MsString					userName;
		/** @brief 头像地址 */
		MsString					headImg;
		/** @brief 是否有负载消息 */
		bool							hasPayload;
		/** @brief 负载消息内容 */
		MsString					payload;

		MsGamePeerJoinNotice() : roomID(0), userID(0), hasPayload(false) {}
	} MsGamePeerJoinNotice;

	/** @brief 游戏动作响应消息 */
	typedef struct MsGameActionRsp
	{

		/** @brief 用户ID */
		int								userID;
		/** @brief 动作ID， GameAction枚举值 */
		int								action;
		/** @brief 动作状态（0成功，其他失败） */
		int								status;
		/** @brief 是否有负载消息 */
		bool								hasPayload;
		/** @brief 负载消息内容 */
		MsString						payload;

		MsGameActionRsp() : userID(0), action(0), status(0), hasPayload(false) {}
	} MsGameActionRsp;

	/** @brief 游戏准备消息 */
	typedef struct MsGameReadyRsp
	{
		/** @brief 用户ID */
		int								userID;
		/** @brief 是否有负载消息 */
		bool								hasPayload;
		/** @brief 负载消息内容 */
		MsString						payload;

		MsGameReadyRsp() : userID(0), hasPayload(false) {}
	} MsGameReadyRsp;

	/** @brief 游戏取消准备消息 */
	typedef struct MsGameReadyRsp MsGameCancelReadyRsp;

	/** @brief 游戏开始消息 */
	typedef struct MsGameStartNotice
	{
		/** @brief 房间ID */
		int								roomID;
		/** @brief 游戏局ID */
		int								roundID;
		/** @brief 用户ID */
		int								userID;
		/** @brief 是否有负载消息 */
		bool								hasPayload;
		/** @brief 负载消息内容 */
		MsString						payload;

		MsGameStartNotice() : roomID(0), roundID(0), userID(0), hasPayload(false) {}
	} MsGameStartNotice;

	/** @brief 游戏结束消息 */
	typedef struct MsGameOverNotice
	{
		/** @brief 房间ID */
		int									roomID;
		/** @brief 游戏局ID */
		int									roundID;
		/** @brief 用户ID */
		int									userID;
		/** @brief 是否有负载消息 */
		bool									hasPayload;
		/** @brief 负载消息内容 */
		MsString							payload;

		MsGameOverNotice() : roomID(0), roundID(0), userID(0), hasPayload(false) {}
	} MsGameOverNotice;


    typedef struct MsFieldInfo
    {
        Int32 FieldId;
        MsFieldInfo()
        {
            memset(this, 0, sizeof(MsFieldInfo));
        }
    }MsFieldInfo;

    typedef struct MsGameOverInfo
    {
        Int32 roundId;
        MsGameOverInfo()
        {
            memset(this, 0, sizeof(MsGameOverInfo));
        }
    }MsGameOverInfo;

    typedef struct MsMatchRobotNotify
    {
        Int32		count;
        MsMatchRobotNotify()
        {
            memset(this, 0, sizeof(MsMatchRobotNotify));
        }
    }MsMatchRobotNotify;


    typedef struct MsRobotInfo
    {
        Int32		userId;					//用户id
        Int32		value;					//所带物品/属性 值
        MsRobotInfo()
        {
            memset(this, 0, sizeof(MsRobotInfo));
        }
    }MsRobotInfo;


    typedef struct MsRobotScore {
        Int32							userId;
        Int32							roomId;
        Int32 							a;
        Int32 							b;
        Int32 							c;
        Int32							exNum;
        Int32							exten[10];
        MsRobotScore()
        {
            memset(this, 0, sizeof(MsRobotScore));
        }
    }MsRobotScore;

    typedef struct MsSettlementNotify
    {
        Int32		roundId;
        Int32       scoreCount;
        MsRobotScore       scores[32];
        MsSettlementNotify()
        {
            memset(this, 0, sizeof(MsSettlementNotify));
        }
    }MsSettlementNotify;
}
#endif//__MATCH_VS_STRUCT_H__

