package code.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 提取成员变量
 * @author GeLiang
 *
 */
public class Field extends Snippet{
	/**
	 * 变量的类型,如int/long/string
	 */
	public String type;
	@Override
	public List<Field> regularExpressionMatch(String source) {
		final List<Field> list = new ArrayList<Field>();
		StringBuilder sb = new StringBuilder();
		sb.append("([a-z_][a-z0-9_\\[\\]]*?)");
		sb.append("\\s+");
		sb.append("([a-z_][a-z0-9_\\[\\]]*?)\\s*;");
		
		Pattern p = Pattern.compile(sb.toString(),
				Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher m = p.matcher(source);
		while (m.find()) {
			Field snippet = new Field();
			snippet.type = m.group(1);
			snippet.source = m.group(0);
			snippet.name = m.group(2);
			
			list.add(snippet);
			System.out.println(snippet.name);
			System.out.println(snippet.type);
			System.out.println("_________");
		}
		return list;
	}
	
}
