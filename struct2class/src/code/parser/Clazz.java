package code.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Clazz extends Snippet{
	
	public String namespace ;
	public boolean isAbstractClass = false;
	@Override
	public  List<Clazz> regularExpressionMatch(String source) {
		StringBuilder sb = new StringBuilder();
		sb.append("class([\\s\\S]*?)\\{([\\s\\S]*)\\}"); // 0
		Pattern p = Pattern.compile(sb.toString(),Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
		Matcher m = p.matcher(source);
		List<Clazz> namespaces = new ArrayList<Clazz>();
		while (m.find()) {
			Clazz sp = new Clazz();
			sp.source = source;
			if (sp.source.contains("virtual")) {
				sp.isAbstractClass = true;
			}
			sp.name = m.group(1).trim();
			sp.snippet = m.group(2);
			namespaces.add(sp);
			System.out.println("_______________________");
			System.out.println("name:"+sp.name);
//			System.out.println("snippet:"+sp.snippet);
			System.out.println("_______________________");
		}
		return namespaces;
	}
}
