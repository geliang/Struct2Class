package code.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameSpace extends Snippet {

	public NameSpace(File file) {
		super(file);
	}

	public NameSpace(String source) {
		super(source);
	}

	public NameSpace() {
		super();
	}

	public  List<NameSpace> regularExpressionMatch(String source) {
		StringBuilder sb = new StringBuilder();
		sb.append("namespace([\\s\\S]*?)\\{([\\s\\S]*)\\}"); // 0
		Pattern p = Pattern.compile(sb.toString());
		Matcher m = p.matcher(source);
		ArrayList<NameSpace> namespaces = new ArrayList<NameSpace>();
		while (m.find()) {
			NameSpace sp = new NameSpace(source);
			sp.name = m.group(1).trim();
			sp.snippet = m.group(2);
			namespaces.add(sp);
		}
		System.out.println("_______________________");
		return namespaces;
	}
}
