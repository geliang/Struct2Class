package code.generation;

import code.parser.Struct;

public interface FieldSerialize extends Serialize{
	public String serialize(Struct struct);
}
