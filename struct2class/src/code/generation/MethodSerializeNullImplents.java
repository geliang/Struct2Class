package code.generation;

import code.parser.Clazz;
import code.parser.Method;
import code.parser.Method.Param;

/**
 * 生成空实现代码
 * @author GeLiang
 *
 */
public class MethodSerializeNullImplents implements MethodSerialize{

	@Override
	public String call(Method method) {

		StringBuilder sb = new StringBuilder();
		if (!method.isVirtual) {
			sb.append("{");
			sb.append("\n        //TODO Code Auto Generation,Null Implements.\n");
			if ("void".equals(method.returnType)) {
				sb.append("return ;");
			} else if ("int".equals(method.returnType)
					|| "long".equals(method.returnType)
					|| "short".equals(method.returnType)
					|| "byte".equals(method.returnType)) {
				sb.append("        return 0;\n");
			} else {
				sb.append("        return null;\n");
			}
			sb.append("    }\n");
		}else{
			sb.append(";\n");
		}
		return sb.toString();
	}

	@Override
	public String callback(Method method) {

		StringBuilder sb = new StringBuilder();
		if (!method.isVirtual) {
			sb.append("{");
			sb.append("\n        //TODO Code Auto Generation,Null Implements.\n");
			if ("void".equals(method.returnType)) {
				sb.append("return ;");
			} else if ("int".equals(method.returnType)
					|| "long".equals(method.returnType)
					|| "short".equals(method.returnType)
					|| "byte".equals(method.returnType)) {
				sb.append("        return 0;\n");
			} else {
				sb.append("        return null;\n");
			}
			sb.append("    }\n");
		}else{
			sb.append(";\n");
		}
		return sb.toString();
	
	}

	@Override
	public String include(Clazz clazz) {
		
		return "\n\n";
	}

	@Override
	public String sign(Method method) {

		StringBuilder sb = new StringBuilder();
		sb.append(TAB_CHAR);
		sb.append(method.access);
		sb.append(" ");
		sb.append(method.isVirtual ? VIRTUAL : "");
		sb.append(" ");
		sb.append(method.returnType);
		sb.append(" ");
		sb.append(method.name);
		sb.append("(");
		// gen params
		if (method.params != null) {
			for (int l = 0; l < method.params.size(); l++) {
				if (l != 0) {
					sb.append(",");
				}
				Param parm = method.params.get(l);
				sb.append(parm.isConst ? CONST : POINT);
				sb.append(" ");
				String paramType = parm.type.replace("&", REF);
				paramType = paramType.replace("*", ARRAY);
				paramType = paramType.replace("MsString", CSTRING);
				sb.append(paramType);
				sb.append(" ");
				sb.append(parm.name);

			}
		}
		sb.append(")");
		return sb.toString();
	
	}

	@Override
	public String callbackSign() {
		// TODO Auto-generated method stub
		return null;
	}

}
