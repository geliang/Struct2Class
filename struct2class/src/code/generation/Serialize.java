package code.generation;

public interface Serialize {
	/**
	 * 空格
	 */
	public static String SPACE_CHAR= " ";
	/**
	 * 用4个空格代替一个tab符号
	 */
	public static String TAB_CHAR= "    ";
	/**
	 * 等价于2个\t符号
	 */
	public static String TAB_CHAR2= "        ";
	/**
	 * 等价于3个\t符号
	 */
	public static String TAB_CHAR3= "            ";
	/**
	 * 等价于4个\t符号
	 */
	public static String TAB_CHAR4= "                ";
	/**
	 * 换行符
	 */
	public static String LINE_CHAR= "\n";
	/**
	 * 换行符
	 */
	public static String LINE_CHAR2= "\n\n";
	/**
	 * 换行符
	 */
	public static String LINE_CHAR3= "\n\n\n";
}
