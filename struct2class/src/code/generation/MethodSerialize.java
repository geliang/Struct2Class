package code.generation;

import code.parser.Clazz;
import code.parser.Method;

/**
 * 序列化方法的调用和回调,返回生成的序列化代码片段
 * @author GeLiang
 *
 */
public interface MethodSerialize extends Serialize{
	
	
	/**
	 * 内部回调函数反序列化方法实现的方法后缀
	 */
	static final String _RPC = "_RPC";
	static final String POINT = "";
	static final String REF = "";
	static final String CONST = "final";
	static final String VIRTUAL = "abstract";
	static final String METHOD_NAME = "action";
	static final String CSTRING = "String";
	static final String ARRAY = "[]";
	
	/**
	 * 生成调用代码
	 * @param method
	 * @return
	 */
	public String call(Method method);
	/**
	 * 生成方法签名,如 public int main(String[] args)
	 * @param method
	 * @return
	 */
	public String sign(Method method);
	/**
	 * 生成回调代码
	 * @param method
	 * @return
	 */
	public String callback(Method method);
	/**
	 * 生成依赖的头文件/第三方库
	 * @param clazz
	 * @return
	 */
	public String include(Clazz clazz);
	/**
	 * 返回回调方法的签名和反序列头
	 * @return
	 */
	public String callbackSign();
}
