#ifndef __MATCH_VS_RESPONSE_H__
#define __MATCH_VS_RESPONSE_H__

#include "MatchVSDefines.h"

namespace matchvs
{
	/** @brief 应答通知接口类(基类)  */
	class  DLL_API  MatchVSResponse
	{
	public:
		MatchVSResponse() {};
		virtual ~MatchVSResponse() {};
	public:
		/**
		* @brief 登陆 回调
		* @param tRsp 登陆结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int loginResponse(const MsLoginRsp& tRsp) = 0;
		/**
		* @brief 登陆 回调
		* @param tRsp 登出结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int logoutResponse(const MsLogoutRsp& tRsp) = 0;
		/**
		* @brief 房间创建 回调
		* @param tRsp 房间创建结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int roomCreateResponse(const MsRoomCreateRsp& tRsp) = 0;
		/**
		* @brief 进入房间 回调
		* @param tRsp 进入 结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int roomJoinResponse(const MsRoomJoinRsp& tRsp) = 0;
		/**
		* @brief 退出房间返回， 回调
		* @param tRsp 退出 结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int roomLeaveResponse(const MsRoomLeaveRsp& tRsp) = 0;

		/**
		* @brief 当前用户游戏动作返回，回调
		* @param tRsp 退出 结果
		* @return MATCHVS_OK: 正确，其他值：错误
		* @note: ready, start, cancelReady, over等动作的确认响应
		*/
		virtual int gameActionResponse(const MsGameActionRsp& tRsp) = 0;

		/**
		* @brief 退出房间 回调
		* @param tRsp 退出 结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int gameStartNotify(const MsGameStartNotice& tRsp) = 0;

		/**
		* @brief 退出房间 回调
		* @param tRsp 退出 结果
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int gameOverNotify(const MsGameOverNotice& tRsp) = 0;

		/**
		* @brief 仲裁分数通知
		* @param tRsp 仲裁分数
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int busiScoresNotify(const MsGameScoreNotice& tRsp) = 0;

		/**
		* @brief 回调指针类型，用于请求用户信息通知
		* @param tRsp 用户信息
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int busiMsgNotify(const MsBusiMsgRsp& tRsp) = 0;

		/**
		* @brief 房间状态同步通知
		* @param tRsp 房间状态
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int roomStatusSyncNotify(const MsUserSyncStatus* pStatusList, int iStatusNum) = 0;
		
		/**
		* @brief 加入房间通知
		* @param tRsp 加入房间通知内容
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int roomPeerJoinNotify(const MsGamePeerJoinNotice& objPeerJoin) = 0;

		/**
		* @brief 其他玩家加入房间通知
		* @param tRsp 加入房间通知内容
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int roomPeerLeaveNotify(const MsRoomLeaveNotice& tRsp) = 0;

		/**
		* @brief 其他玩家游戏动作通知
		* @note 比如UserState枚举： ready, start, cancelReady, over等变化
		* @param  tRsp 用户状态
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int gamePeerActionNotify(const MsUserSyncStatus& tRsp) = 0;

		/**
		* @brief 错误消息回调
		* @param sError  错误消息
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		virtual int errorResponse(const char* sError) = 0;

        /** @brief 机器人加入下发通知   */
        virtual int busiRobotJoinNotify(const MsMatchRobotNotify& objMsMatchRobotNotify, MsRobotInfo* objRobotInfoList) = 0 ;
        /** @brief  结算分数仲裁分数下发通知   */
        virtual int busiSettlementNotify(const MsSettlementNotify& objMsSettlementNotify) = 0;

	};
}

#endif
