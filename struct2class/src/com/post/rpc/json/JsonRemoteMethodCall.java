package com.post.rpc.json;

public class JsonRemoteMethodCall {
	public static IJsonRemoteMethodCall jsonRemoteMethodCall;
	public static int callJsonMethod(String json){
		if (jsonRemoteMethodCall!=null) {
			return jsonRemoteMethodCall.callJsonMethod(json);
		}
		return 0;
	}
	public static  void setRemoteMethodImp(IJsonRemoteMethodCall rmc){
		if (rmc!=null) {
			jsonRemoteMethodCall = rmc;
			
		}
	}
}
