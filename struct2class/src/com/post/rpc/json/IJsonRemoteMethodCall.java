package com.post.rpc.json;

public interface IJsonRemoteMethodCall {
	public int callback(String source);
	public int callJsonMethod(String json);
}
