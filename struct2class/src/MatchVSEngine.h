#ifndef __MATCHVS_APP_H__
#define __MATCHVS_APP_H__

#include "MatchVSDefines.h"
#include <MatchVSResponse.h>

namespace matchvs
{
	/** @brief MatchVS SDK用户接口（API）  */
	class DLL_API MatchVSEngine
	{

	private:
		MatchVSEngine(void);
		MatchVSEngine(const MatchVSEngine& obj);
		MatchVSEngine& operator=(const MatchVSEngine& obj);

	public:
		/**
		* @brief 获取MatchVSApp
		* @return MatchVSApp*
		*/
		static MatchVSEngine* getInstance();

		/**
		* @brief 初始化基类指针
		* @param MatchVSResponse* 派生类指针
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int init(MatchVSResponse* pMatchVSResponse);

		/**
		* @brief 登陆游戏
		* @param iGameId 游戏id, iVersion 游戏版本号, pAppKey 游戏App key
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int login(int iGameId, int iVersion, const char* pAppKey);

		/**
		* @brief 获取当前用户信息， 登录后获取用户详细信息
		* @param objUserinfo用户信息 （输出）
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int getUserInfo(MsUserInfo& objUserinfo);

		/**
		* @brief 获取房间所有用户信息列表， 前置条件：当前用户在房间
		* @param ppUsersList 用户信息指针地址（二级指针），iUserNum 用户个数  （ 都为输出参数 ）
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int getRoomUserInfoList(MsUserInfo **ppUsersList, int &iUserNum);

		/**
		* @brief 退出游戏
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int logout();

		/**
		* @brief 创建房间
		* @param sName 房间名
		* @param iMaxUsers 最大用户数
		* @param iFlag 标签
		* @param sPayload 负载数据
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int createRoom(const MsString& sName, int iMaxUsers, int iFlag, const MsString& sPayload);

		/**
		* @brief 进入房间
		* @param sName 房间名
		* @param iFieldId 场id (0无效)
		* @param iFlag 标签
		* @param sPayload 负载数据
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int joinRoom(const MsString& sName, int iFieldId, int iFlag, const MsString& sPayload = "");

		/**
		* @brief 退出房间
		* @param sPayload 负载数据
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int leaveRoom(const MsString& sPayload = "");

		/**
		* @brief 发送消息
		* @param 事件类型
		* @param iTargetNum 用户id数量, 0表示房间所有用户
		* @param pTargetUserId 用户id串
		* @param iMsgSize 消息长度
		* @param pMsg 消息内容
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int sendEvent(const int iEvent, const int iTargetNum, int* pTargetUserId, const int iMsgSize, const char* pMsg);


	public:/****************   game pulgin api ************************/
		/**
		* @brief 游戏准备
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int gameReady();

		/**
		* @brief 游戏取消准备
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int gameCancelReady();

		/**
		* @brief 游戏开始
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int gameStart();

		/**
		* @brief 游戏结束
		* @return MATCHVS_OK: 正确，其他值：错误
		*/
		int gameOver(const MsGameScore& objScore);

        /**
        * @brief 上报机器人分数
        * @param objScore 机器人分数信息 
        * @return MATCHVS_OK: 正确，其他值：错误
        */
        int reportRobotScore(const MsRobotScore& objScore);
	private:
		MatchVSResponse*		m_pRspCallback;
		void*							m_pBusi;
	};
}


#endif//__MATCHVS_MSG_CLIENT_H__
