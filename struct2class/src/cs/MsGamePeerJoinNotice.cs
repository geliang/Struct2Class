namespace MatchVS {

public  class MsGamePeerJoinNotice{
	
		/** @brief 房间ID */
		public int roomID;
		/** @brief 用户ID */
		public int userID;
		/** @brief 用户名字 */
		public string userName;
		/** @brief 头像地址 */
		public string headImg;
		/** @brief 是否有负载消息 */
		public bool							hasPayload;
		/** @brief 负载消息内容 */
		public string payload;

		

	}
}