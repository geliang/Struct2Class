namespace MatchVS {

public  class MsBusiMsg{
	
		/** @brief 目标用户数 */
		public byte						targetUserNum;
		/** @brief 标签 */
		public byte						flags;
		/** @brief 事件ID */
		public int eventID;
		/** @brief 目标用户 */
		public int []targetUsers = new int[128];
		/** @brief 消息大小 */
		public int msgSize;
		/** @brief 消息内容 */
		public string msg;
		/** @brief 是否有分数 */
		public bool							hasScore;
		/** @brief 分数(由hasScore决定) */
		MsGameScore			score;

		

	}
}