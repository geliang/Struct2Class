namespace MatchVS {

public  class MsGameOverNotice{
	
		/** @brief 房间ID */
		public int roomID;
		/** @brief 游戏局ID */
		public int roundID;
		/** @brief 用户ID */
		public int userID;
		/** @brief 是否有负载消息 */
		public bool									hasPayload;
		/** @brief 负载消息内容 */
		public string payload;

		

	}
}