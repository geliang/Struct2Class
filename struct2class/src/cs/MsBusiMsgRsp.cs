namespace MatchVS {

public  class MsBusiMsgRsp{
	
		/** @brief 用户id */
		public int userId;
		/** @brief 标签 */
		public byte							flags;
		/** @brief 事件ID */
		public int eventID;
		/** @brief 消息大小 */
		public int payloadSize;
		/** @brief 是否有负载消息 */
		public bool							hasPayload;
		/** @brief 消息内容 */
		public string payload;
		/** @brief 是否有分数 */
		public bool							hasScore;
		/** @brief 分数 */
		MsGameScoreNotice	score;

		

	}
}