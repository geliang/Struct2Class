namespace MatchVS {

public  class MsUserInfo{
	
		/** @brief 用户id */
		public int userID;
		/** @brief 用户的验证授权 */
		public string token;
		/** @brief 用户昵称 */
		public string nickname;
		/** @brief 用户头像地址 */
		public string headerUri;
		
		

	}
}