namespace MatchVS {

public  class MsUserSyncStatus{
	

		/** @brief 房间ID */
		public int roomId;
		/** @brief 用户ID */
		public int userId;
		/** @brief 房主ID */
		public int ownerId;
		/** @brief 角色ID */
		public int roleId;
		/** @brief 用户状态 */
		public int status;
		/** @brief 状态标识 */
		public int flags;

		

	}
}