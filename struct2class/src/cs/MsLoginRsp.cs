namespace MatchVS {

public  class MsLoginRsp{
	
		/** @brief 登陆状态 */
		public int status;
		/** @brief 用户ID */
		public int userID;
		/** @brief 用户名 */
		public string name;
		/** @brief 头像地址 */
		public string headerUri;
		
		

	}
}