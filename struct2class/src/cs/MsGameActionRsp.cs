namespace MatchVS {

public  class MsGameActionRsp{
	

		/** @brief 用户ID */
		public int userID;
		/** @brief 动作ID， GameAction枚举值 */
		public int action;
		/** @brief 动作状态（0成功，其他失败） */
		public int status;
		/** @brief 是否有负载消息 */
		public bool								hasPayload;
		/** @brief 负载消息内容 */
		public string payload;

		

	}
}