namespace MatchVS; 


public abstract class MatchVSResponse{ 

    public virtual void loginResponse(const MsLoginRsp ref  loginRsp);
    public virtual void logoutResponse(const MsLogoutRsp ref  logoutRsp);
    public virtual void userStateChangedNotify(const MsUserSyncStatus[] userSyncStatusList, int userSyncStatusListSize);
    public virtual void userJoinRoomResponse(const MsRoomJoinRsp ref  roomJoinRsp);
    public virtual void userJoinRoomNotify(const MsRoomJoinNotice ref  gameUserJoinNotice);
    public virtual void userLeaveRoomResponse(const MsRoomLeaveRsp ref  roomLeaveRsp);
    public virtual void userLeaveRoomNotify(const MsRoomLeaveNotice ref  roomLeaveNotice);
    public virtual void gameReadyNotify(const MsGameReadyNotice ref  gameReadyNotice);
    public virtual void gameReadyResponse(const MsGameReadyRsp ref  gameReadyRsp);
    public virtual void gameCancelReadyNotify(const MsGameCancelReadyNotice ref  gameCancelReadyNotice);
    public virtual void gameCancelReadyResponse(const MsGameCancelReadyRsp ref  gameCancelReadyRsp);
    public virtual void gameStartNotify(const MsGameStartNotice ref  gameStartNotice);
    public virtual void gameOverNotify(const MsGameOverNotice[] gameOverNoticeList, int gameOverNoticeListSize);
    public virtual void messageNotify(const MsBusiMsgRsp ref  busiMsgRsp);
    public virtual void errorResponse( int errCode,const string ref  errorInfo);

}