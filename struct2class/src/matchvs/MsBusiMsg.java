/* ******************************** */
/* Code Generation By @struct2class */
/* ******************************** */
package   matchvs;
public  class MsBusiMsg{
    public char targetUserNum;
    public char flags;
    public int eventID;
    public int targetUsers[] = new int[128];
    public int msgSize;
    public String msg;
    public boolean hasScore;
    public MsGameScore score;
}