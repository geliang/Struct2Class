/* ******************************** */
/* Code Generation By @struct2class */
/* ******************************** */
package   matchvs;
public  class MsBusiMsgRsp{
    public int userId;
    public char flags;
    public int eventID;
    public int payloadSize;
    public boolean hasPayload;
    public String payload;
    public boolean hasScore;
    public MsGameScoreNotice score;
}