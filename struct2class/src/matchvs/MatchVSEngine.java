/* ******************************** */
/* Code Generation By @struct2class */
/* ******************************** */
package matchvs; 


import org.json.*;
import com.google.gson.Gson;
import com.post.rpc.json.*;


public  class MatchVSEngine{ 

    public  int init( MatchVSResponse[] pMatchVSResponse){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","init");
            jo.put("pMatchVSResponse", new JSONArray(new Gson().toJson(pMatchVSResponse)));
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int uninit(){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","uninit");
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int login( int iGameId, int iVersion,String pAppKey){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","login");
            jo.put("iGameId", iGameId);
            jo.put("iVersion", iVersion);
            jo.put("pAppKey", new JSONArray(new Gson().toJson(pAppKey)));
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int getUserInfo( MsUserInfo objUserinfo){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","getUserInfo");
            jo.put("objUserinfo", new JSONObject(new Gson().toJson(objUserinfo)));
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
//    public  int getRoomUserInfoList( [][]ppUsersList, int){
//        try {
//            JSONObject jo = new JSONObject();
//            jo.put("action","getRoomUserInfoList");
//            jo.put("int", new JSONArray(new Gson().toJson(int)));
//            JsonRemoteMethodCall.callJsonMethod(jo.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }
    public  int logout(){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","logout");
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int createRoom(final String sName, int iMaxUsers, int iFlag,final String sPayload){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","createRoom");
            jo.put("sName", sName);
            jo.put("iMaxUsers", iMaxUsers);
            jo.put("iFlag", iFlag);
            jo.put("sPayload", sPayload);
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int joinRoom(final String sName, int iFieldId, int iFlag,final String sPayload){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","joinRoom");
            jo.put("sName", sName);
            jo.put("iFieldId", iFieldId);
            jo.put("iFlag", iFlag);
            jo.put("sPayload", sPayload);
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int leaveRoom(final String sPayload){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","leaveRoom");
            jo.put("sPayload", sPayload);
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int sendEvent(final int iEvent,final int iTargetNum, int[] pTargetUserId,final int iMsgSize,final char[] pMsg){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","sendEvent");
            jo.put("iEvent", iEvent);
            jo.put("iTargetNum", iTargetNum);
            jo.put("pTargetUserId", new JSONArray(new Gson().toJson(pTargetUserId)));
            jo.put("iMsgSize", iMsgSize);
            jo.put("pMsg", new JSONArray(new Gson().toJson(pMsg)));
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int gameReady(){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","gameReady");
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int gameCancelReady(){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","gameCancelReady");
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int gameStart(){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","gameStart");
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int gameOver(final MsGameScore objScore){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","gameOver");
            jo.put("objScore", new JSONObject(new Gson().toJson(objScore)));
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public  int reportRobotScore(final MsRobotScore objScore){
        try {
            JSONObject jo = new JSONObject();
            jo.put("action","reportRobotScore");
            jo.put("objScore", new JSONObject(new Gson().toJson(objScore)));
            JsonRemoteMethodCall.callJsonMethod(jo.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}