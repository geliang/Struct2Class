/* ******************************** */
/* Code Generation By @struct2class */
/* ******************************** */
package   matchvs;
public  class MsUserSyncStatus{
    public int roomId;
    public int userId;
    public int ownerId;
    public boolean isRobot;
    public int status;
    public int robotGrade;
}