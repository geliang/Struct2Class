/* ******************************** */
/* Code Generation By @struct2class */
/* ******************************** */
package   matchvs;
public  class MsGameActionRsp{
    public int userID;
    public int action;
    public int status;
    public boolean hasPayload;
    public String payload;
}