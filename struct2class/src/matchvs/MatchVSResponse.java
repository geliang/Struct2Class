/* ******************************** */
/* Code Generation By @struct2class */
/* ******************************** */
package matchvs; 


import org.json.*;
import com.google.gson.Gson;
import com.post.rpc.json.*;


public abstract class  MatchVSResponse{ 

    public abstract int loginResponse(final MsLoginRsp tRsp);
    public abstract int logoutResponse(final MsLogoutRsp tRsp);
    public abstract int roomCreateResponse(final MsRoomCreateRsp tRsp);
    public abstract int roomJoinResponse(final MsRoomJoinRsp tRsp);
    public abstract int roomLeaveResponse(final MsRoomLeaveRsp tRsp);
    public abstract int gameActionResponse(final MsGameActionRsp tRsp);
    public abstract int gameStartNotify(final MsGameStartNotice tRsp);
    public abstract int gameOverNotify(final MsGameOverNotice tRsp);
    public abstract int busiScoresNotify(final MsGameScoreNotice tRsp);
    public abstract int busiMsgNotify(final MsBusiMsgRsp tRsp);
    public abstract int roomStatusSyncNotify(final MsUserSyncStatus[] pStatusList, int iStatusNum);
    public abstract int roomPeerJoinNotify(final MsGamePeerJoinNotice objPeerJoin);
    public abstract int roomPeerLeaveNotify(final MsRoomLeaveNotice tRsp);
    public abstract int gamePeerActionNotify(final MsUserSyncStatus tRsp);
    public abstract int errorResponse(final char[] sError);
    public abstract int busiRobotJoinNotify(final MsMatchRobotNotify objMsMatchRobotNotify, MsRobotInfo[] objRobotInfoList);
    public abstract int busiSettlementNotify(final MsSettlementNotify objMsSettlementNotify);
    public int callback(String source){
        try {
            JSONObject jo = new JSONObject(source);
            String methodName = jo.getString("action");
            if(methodName.equals("loginResponse")){
                MsLoginRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsLoginRsp.class);

                loginResponse(tRsp);
                return 0;
            }

            if(methodName.equals("logoutResponse")){
                MsLogoutRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsLogoutRsp.class);

                logoutResponse(tRsp);
                return 0;
            }

            if(methodName.equals("roomCreateResponse")){
                MsRoomCreateRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsRoomCreateRsp.class);

                roomCreateResponse(tRsp);
                return 0;
            }

            if(methodName.equals("roomJoinResponse")){
                MsRoomJoinRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsRoomJoinRsp.class);

                roomJoinResponse(tRsp);
                return 0;
            }

            if(methodName.equals("roomLeaveResponse")){
                MsRoomLeaveRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsRoomLeaveRsp.class);

                roomLeaveResponse(tRsp);
                return 0;
            }

            if(methodName.equals("gameActionResponse")){
                MsGameActionRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsGameActionRsp.class);

                gameActionResponse(tRsp);
                return 0;
            }

            if(methodName.equals("gameStartNotify")){
                MsGameStartNotice tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsGameStartNotice.class);

                gameStartNotify(tRsp);
                return 0;
            }

            if(methodName.equals("gameOverNotify")){
                MsGameOverNotice tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsGameOverNotice.class);

                gameOverNotify(tRsp);
                return 0;
            }

            if(methodName.equals("busiScoresNotify")){
                MsGameScoreNotice tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsGameScoreNotice.class);

                busiScoresNotify(tRsp);
                return 0;
            }

            if(methodName.equals("busiMsgNotify")){
                MsBusiMsgRsp tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsBusiMsgRsp.class);

                busiMsgNotify(tRsp);
                return 0;
            }

            if(methodName.equals("roomStatusSyncNotify")){
                MsUserSyncStatus[] pStatusList = new Gson().fromJson(jo.get("pStatusList").toString(), MsUserSyncStatus[].class);

                int iStatusNum = (Integer)jo.get("iStatusNum");

                roomStatusSyncNotify(pStatusList,iStatusNum);
                return 0;
            }

            if(methodName.equals("roomPeerJoinNotify")){
                MsGamePeerJoinNotice objPeerJoin = new Gson().fromJson(jo.get("objPeerJoin").toString(), MsGamePeerJoinNotice.class);

                roomPeerJoinNotify(objPeerJoin);
                return 0;
            }

            if(methodName.equals("roomPeerLeaveNotify")){
                MsRoomLeaveNotice tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsRoomLeaveNotice.class);

                roomPeerLeaveNotify(tRsp);
                return 0;
            }

            if(methodName.equals("gamePeerActionNotify")){
                MsUserSyncStatus tRsp = new Gson().fromJson(jo.get("tRsp").toString(), MsUserSyncStatus.class);

                gamePeerActionNotify(tRsp);
                return 0;
            }

            if(methodName.equals("errorResponse")){
                char[] sError = new Gson().fromJson(jo.get("sError").toString(), char[].class);

                errorResponse(sError);
                return 0;
            }

            if(methodName.equals("busiRobotJoinNotify")){
                MsMatchRobotNotify objMsMatchRobotNotify = new Gson().fromJson(jo.get("objMsMatchRobotNotify").toString(), MsMatchRobotNotify.class);

                MsRobotInfo[] objRobotInfoList = new Gson().fromJson(jo.get("objRobotInfoList").toString(), MsRobotInfo[].class);

                busiRobotJoinNotify(objMsMatchRobotNotify,objRobotInfoList);
                return 0;
            }

            if(methodName.equals("busiSettlementNotify")){
                MsSettlementNotify objMsSettlementNotify = new Gson().fromJson(jo.get("objMsSettlementNotify").toString(), MsSettlementNotify.class);

                busiSettlementNotify(objMsSettlementNotify);
                return 0;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

}