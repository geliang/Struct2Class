#Struct2Class

## 简介
* 根据.h文件生成.java和.cs文件 
* 将c/c++语法中结构体 从语法上转换成java或c#的类

* struct to class javacs
* .h to .java/.cs


## 使用示例
```java
public static void main(String[] args) throws IOException {
		struct2class(
				new File("D:\\workspaceWeb\\c2java\\src\\MatchVSDefines.h"),
				new File("D:\\workspaceWeb\\c2java\\src\\cs\\"), ".cs");
		struct2class(
				new File("D:\\workspaceWeb\\c2java\\src\\MatchVSDefines.h"),
				new File("D:\\workspaceWeb\\c2java\\src\\"), ".java");
}
```

## 原理


基于正则表达式进行语法分析